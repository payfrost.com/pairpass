use num_bigint::BigInt;
use pairpass::*;

fn main()
{
/*
    let x = FieldElement{num: BigInt::parse_bytes(b"4",10).unwrap(), prime: BigInt::parse_bytes(b"31",10).unwrap()};

    let z = FieldElement{num: BigInt::parse_bytes(b"11",10).unwrap(), prime: BigInt::parse_bytes(b"31",10).unwrap() };

    println!("{}", x.modpow( BigInt::parse_bytes(b"-4",10).unwrap() ) * z );

*/
    let p: BigInt = BigInt::parse_bytes(b"11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",2).unwrap();

    let secret: BigInt = BigInt::parse_bytes(b"00829477a80aaea4b6a6f55222a068c960dad26a545e8f6d54df7fb1b43c4bdb",16).unwrap();

    let ans_arr: [Vec<BigInt>;3] = shamir_format_piece_arr(3,5,256,p.clone(),secret.clone());

    let mut x_vec: Vec<BigInt> = (ans_arr[1]).clone();

    let mut pieces: Vec<BigInt> = (ans_arr[2]).clone();

    let mut reconstruct_secret : BigInt = reconstruction(3,p.clone(),(x_vec.clone()[2..5]).to_vec(),(pieces.clone()[2..5]).to_vec());
    
//    let reconstruct_secret : BigInt = reconstruction(3,p.clone(),(x_vec.clone()[1..4]).to_vec(),(pieces.clone()[1..4]).to_vec());

//    let reconstruct_secret : BigInt = reconstruction(3,p.clone(),(x_vec.clone()[2..5]).to_vec(),(pieces.clone()[2..5]).to_vec());

    println!("reconstructed_secret: {:#?}\n",reconstruct_secret == BigInt::parse_bytes(b"00829477a80aaea4b6a6f55222a068c960dad26a545e8f6d54df7fb1b43c4bdb",16).unwrap());    
    
    let a_vec: Vec<BigInt> = (ans_arr[0]).clone();

    let replacement_piece: (BigInt,BigInt) = replacement(3,p.clone(),256,a_vec.clone(),x_vec.clone(),pieces.clone());

//    println!("replacement_piece: ({:#?},{:#?})\n",replacement_piece.0,replacement_piece.1);

    x_vec[4] = replacement_piece.0;

    pieces[4] = replacement_piece.1;

    reconstruct_secret = reconstruction(3,p.clone(),(x_vec.clone()[2..5]).to_vec(),(pieces.clone()[2..5]).to_vec());
    
    println!("reconstructed_secret: {:#?}\n",reconstruct_secret == BigInt::parse_bytes(b"00829477a80aaea4b6a6f55222a068c960dad26a545e8f6d54df7fb1b43c4bdb",16).unwrap());    

}

