//extern crate libc;
use libc::c_void;
use libsodium_sys::randombytes_buf;
use num_bigint::BigInt;
use std::fmt;
use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;
use std::ops::Div;
use std::ops::BitXor;
use std::cmp::PartialEq;

pub struct FieldElement
{
    num: BigInt,

    prime: BigInt

}

impl FieldElement
{
    pub fn new(n: BigInt,p: BigInt) -> FieldElement
    {
        FieldElement { num: n , prime: p }
    }

// modpow must work for negative exponents as well

    pub fn modpow(&self,mut exp: BigInt) -> FieldElement
    {

        let mut base: BigInt = self.num.clone() % self.prime.clone();

        let mut result: BigInt = BigInt::parse_bytes(b"1",10).unwrap();

        if exp.clone() < BigInt::parse_bytes(b"0",10).unwrap()
        {
            exp = exp.clone() + self.prime.clone() - BigInt::parse_bytes(b"1",10).unwrap();
        }

        while exp > BigInt::parse_bytes(b"0",10).unwrap()
        {
            if exp.clone() % BigInt::parse_bytes(b"2",10).unwrap()  == BigInt::parse_bytes(b"1",10).unwrap()
            {
                result = ( result.clone() * base.clone() ) % self.prime.clone();

            }

            exp /= BigInt::parse_bytes(b"2",10).unwrap();
            
            base = ( base.clone() * base.clone() ) % self.prime.clone();
        }

        FieldElement
        {
            num: result,

            prime: self.prime.clone()
        }
    }

}

impl fmt::Display for FieldElement
{
     fn fmt(&self, dest: &mut fmt::Formatter) -> fmt::Result
    {
        write!(dest,"FieldElement_{}({})",self.num,self.prime) 
    }
}

impl Add for FieldElement
{
    type Output = FieldElement;

     fn add(self,rhs: Self) -> Self
    {
        let rawsum = self.num + rhs.num;

        FieldElement
        {
            num: rawsum % rhs.prime,

            prime: self.prime
        }
    }

}

impl Sub for FieldElement
{
    type Output = FieldElement;

     fn sub(self,rhs: Self) -> Self
    {
        let rawsum = self.num.clone() - rhs.num.clone();

        let pr = self.prime.clone();

        let cursum = if rawsum < BigInt::parse_bytes(b"0",10).unwrap() { ( self.prime + rawsum ) % rhs.prime } else { self.num.clone() - rhs.num.clone() };

        FieldElement
        {
            num: cursum,

            prime: pr
        }
    }

}

impl Mul for FieldElement
{
    type Output = FieldElement;

     fn mul(self,rhs: Self) -> Self
    {
        let rawproduct = ( self.num.clone() * rhs.num.clone() ) % self.prime.clone();

        FieldElement
        {
            num: rawproduct,

            prime: self.prime.clone()
        }
    }
}

impl BitXor for FieldElement
{
    type Output = FieldElement;

     fn bitxor(self,rhs: Self) -> Self
    {
        let mut base: BigInt = self.num.clone() % self.prime.clone();

        let mut result: BigInt = BigInt::parse_bytes(b"1",10).unwrap();

        let mut exp: BigInt = rhs.num.clone();

        while exp > BigInt::parse_bytes(b"0",10).unwrap()
        {
            if exp.clone() % BigInt::parse_bytes(b"2",10).unwrap()  == BigInt::parse_bytes(b"1",10).unwrap()
            {
                result = ( result.clone() * base.clone() ) % self.prime.clone();

            }

            exp /= BigInt::parse_bytes(b"2",10).unwrap();
            
            base = ( base.clone() * base.clone() ) % self.prime.clone();
        }

        FieldElement
        {
            num: result,

            prime: self.prime.clone()
        }
    }
}


impl Div for FieldElement
{
    type Output = FieldElement;

     fn div(self,rhs: Self) -> Self
    {

        let x: FieldElement = FieldElement{num: self.num.clone(), prime: self.prime.clone() };

        let y: FieldElement = FieldElement{num: rhs.num.clone(), prime: rhs.prime.clone() };

        x * y.modpow( self.prime.clone() - BigInt::parse_bytes(b"2",10).unwrap() )
    }

}



impl PartialEq for FieldElement
{

     fn eq(&self,rhs: &FieldElement) -> bool
    {
        self.num == rhs.num && self.prime == rhs.prime 
    }
    
     fn ne(&self,rhs: &FieldElement) -> bool
    {
        self.num != rhs.num || self.prime != rhs.prime 
    }
}


pub fn base_256_to_hex(input: Vec<u8>) -> Vec<u8>
{
    const HEXTABLE: [u8; 16] = [0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x61,0x62,0x63,0x64,0x65,0x66];

    let mut hexvec: Vec<u8> = Vec::with_capacity(input.len() * 2);

    let mut i: usize = 0;
    
    let mut j: usize;

    while i < input.len()
    {
        j = ( ( input[i] >> 4 ) & 0x0f ) as usize;

        hexvec.push( HEXTABLE[j] );

        j = ( input[i] & 0x0f ) as usize;

        hexvec.push( HEXTABLE[j] );

        i += 1;
    }

    return hexvec;

}

pub fn shamir_gen_constant(entropy: usize, p: BigInt) -> BigInt
{
    let mut span: *mut u8;

    let mut spanlen: usize = BigInt::bits(&p).try_into().unwrap();

    if spanlen <= ( 2 * entropy)
    {
        panic!("Error: {} <= ( 2 * {} )\n",spanlen,entropy);
    }

    if spanlen % 8 != 0
    {
        spanlen = spanlen / 8 + 1;
    }

    else
    {
        spanlen = spanlen / 8;
    }

    let mut ans: BigInt = BigInt::parse_bytes(b"0",10).unwrap();

    while ans == BigInt::parse_bytes(b"0",10).unwrap()
    {

        let mut vecspan: Vec<u8> = Vec::with_capacity(spanlen);

        let spanptr: &[u8];

        unsafe
        {
            span = libc::calloc(spanlen,1) as *mut u8;

            randombytes_buf(span as *mut c_void,spanlen);

            let mut i: isize = 0;

            let mut j: usize = 0;

            while j < spanlen
            {
    //            arrspan[j] = *(span.offset(i));

                vecspan.push( *(span.offset(i)) );

                i += 1;

                j += 1;

            }

    //        println!("arrspan_len: {:?}\n\nvecspan: {:?}",vecspan.len(),vecspan);

        }

        let hexspan: Vec<u8> = base_256_to_hex(vecspan);

        spanptr = &hexspan;

    //    println!("spanptr: {:#?}\n",spanptr);

        ans = BigInt::parse_bytes(spanptr,16).unwrap();

        if ans >= p
        {
            ans %= p.clone();
        }

    }

    return ans;

}


pub fn shamir_gen_piece(k: usize,p: BigInt,a_0: BigInt,x_i: BigInt,a_i: Vec<BigInt>) -> BigInt
{

    let mut calc: FieldElement = FieldElement{ num: a_0.clone() , prime: p.clone() };

    let x_i: FieldElement = FieldElement{ num: x_i.clone(), prime: p.clone() };

/*
 * Shamir Secret Sharing piece generation y = a_0 + a_1 * x + a_2 * x^2 + .. a_k-1 * x^(k-1) mod p
 *
 * The (mod p) denotes Finite Field Arithmetic
*/
    let mut i: usize = 1;
    
    let mut j: BigInt = BigInt::parse_bytes(b"1",10).unwrap();

    while i < (k-1)
    {
        let a_i_fieldelem : FieldElement = FieldElement{ num: (a_i[i]).clone() , prime: p.clone() };

        calc = calc + a_i_fieldelem * x_i.modpow( j.clone() );
        
        i += 1;

        j += BigInt::parse_bytes(b"1",10).unwrap();
    }

    return calc.num;
    
}

// The format of the output array is: <a_1,a_2,...,a_k-1>,<x_1,x_2,x_3,..,x_n-1>,<d_1,d_2,d_3,..,d_n-1>
//

pub fn shamir_format_piece_arr(k: usize,n: usize,entropy: usize,p: BigInt,a_0: BigInt) -> [Vec<BigInt>;3]
{
    if k >= n
    {
        panic!("Error: {} >= {}\n",k,n);
    }

    if p < a_0
    {
        panic!("Error: {} < {}\n",p,a_0);
    }

    let mut result: Vec<BigInt> = Vec::with_capacity(k - 1 + n);
    
    let mut a_vec: Vec<BigInt> = Vec::with_capacity(k-1); 

    let mut x_vec: Vec<BigInt> = Vec::with_capacity(n);

    let mut a_index: usize = 0;

    while a_index < (k-1)
    {
        a_vec.push( shamir_gen_constant( entropy, p.clone() ) );

        a_index += 1;
    }

    a_index = 0;
    
    let mut x_index: usize = 0;

//    let mut a_vec: Vec<BigInt> = result.clone();

//    let mut x_vec: Vec<BigInt> = Vec::with_capacity(n);

    while x_index < n
    {
        x_vec.push( shamir_gen_constant( entropy,p.clone() ) );

        x_index += 1;
    }

    while a_index < n
    {

        result.push( shamir_gen_piece(k,p.clone(),a_0.clone(),(x_vec[a_index]).clone(),a_vec.clone() ) );
         
        a_index += 1;
    }


    a_index = 0;

    while a_index < result.len()
    {
        if result[a_index] >= p.clone()
        {
            panic!("Error: result[a_index] >= p: {:#?} >= {:#?}\n",result[a_index],p.clone());
        }

        a_index += 1;
    }

    return [a_vec,x_vec,result];
}

pub fn reconstruction(k: usize,p: BigInt,x_vec: Vec<BigInt>,pieces: Vec<BigInt>) -> BigInt
{
    let mut secret: FieldElement = FieldElement{ num: BigInt::parse_bytes(b"0",10).unwrap(), prime: p.clone() };

    let mut product: FieldElement;

    let mut x_m: FieldElement;

    let mut x_m_clone: FieldElement;

    let mut x_j: FieldElement;

    let mut y_j: FieldElement;

    let mut j: usize = 0;

    let mut m: usize;
    
    while j <= (k-1)
    {

        m = 0;

        product = FieldElement{ num: BigInt::parse_bytes(b"1",10).unwrap() , prime: p.clone() };

        while m <= (k-1)
        {
            if m == j
            {
                m += 1;

                continue;
            }

            x_m = FieldElement{ num: (x_vec[m]).clone() , prime: p.clone() };
            
            x_m_clone = FieldElement{ num: (x_vec[m]).clone() , prime: p.clone() };

            x_j = FieldElement{ num: (x_vec[j]).clone() , prime: p.clone() };

            product = product * ( x_m / ( x_m_clone - x_j ) );

            m += 1;
        }

        y_j = FieldElement{ num: (pieces[j]).clone() , prime: p.clone() };

        secret = secret + ( y_j * product );

        j += 1;
    }

    return secret.num;
}


pub fn replacement(k: usize,p: BigInt,entropy: usize,a_vec: Vec<BigInt>,x_vec: Vec<BigInt>,pieces: Vec<BigInt>) -> (BigInt,BigInt)
{
    let reconstruct_secret : BigInt = reconstruction(k,p.clone(),x_vec.clone(),pieces.clone());

    let x_i: BigInt = shamir_gen_constant( entropy, p.clone() );

    let y_i: BigInt = shamir_gen_piece( k,p.clone(),reconstruct_secret.clone(),x_i.clone(),a_vec.clone() );

    return (x_i.clone(),y_i.clone());
}

pub fn redefinition(k: usize,n: usize,p: BigInt,entropy: usize,x_vec: Vec<BigInt>,pieces: Vec<BigInt>) -> [Vec<BigInt>;3]
{
    let reconstruct_secret : BigInt = reconstruction(k,p.clone(),x_vec.clone(),pieces.clone());
    
    let new_result: [Vec<BigInt>;3] = shamir_format_piece_arr( k,n,entropy,p.clone(),reconstruct_secret );

    return new_result;
}

